1
00:00:00,080 --> 00:00:04,160
all right let's go ahead and see a break

2
00:00:02,560 --> 00:00:06,640
on right

3
00:00:04,160 --> 00:00:09,519
so to do that i'm going to pick the sort

4
00:00:06,640 --> 00:00:12,719
of random location that I broke before

5
00:00:09,519 --> 00:00:14,559
going to set up a display of 10 assembly

6
00:00:12,719 --> 00:00:17,760
instructions

7
00:00:14,559 --> 00:00:19,039
at rip i'm going to run to that random

8
00:00:17,760 --> 00:00:21,199
location

9
00:00:19,039 --> 00:00:22,080
and here's the assembly I see I want to

10
00:00:21,199 --> 00:00:24,080
see

11
00:00:22,080 --> 00:00:26,080
i want to set a break on right and it

12
00:00:24,080 --> 00:00:27,680
just happens that there is a read from a

13
00:00:26,080 --> 00:00:30,000
memory address here and then there's a

14
00:00:27,680 --> 00:00:32,480
right to a memory address here so

15
00:00:30,000 --> 00:00:35,120
without even knowing what's in rdi I can

16
00:00:32,480 --> 00:00:36,880
actually just specify the address as rdi

17
00:00:35,120 --> 00:00:39,440
so break

18
00:00:36,880 --> 00:00:41,440
percentage rdi

19
00:00:39,440 --> 00:00:43,840
for the address of rdi that's currently

20
00:00:41,440 --> 00:00:45,360
in that register and then dash w for a

21
00:00:43,840 --> 00:00:48,000
break on write

22
00:00:45,360 --> 00:00:50,079
so I set that I could list my break

23
00:00:48,000 --> 00:00:52,000
points now

24
00:00:50,079 --> 00:00:53,840
and I can see that I have my break on

25
00:00:52,000 --> 00:00:56,559
execute which was the random address and

26
00:00:53,840 --> 00:00:58,320
a break on write which it filled in that

27
00:00:56,559 --> 00:00:59,840
you know when I set this break on right

28
00:00:58,320 --> 00:01:04,159
looks like the value that was actually

29
00:00:59,840 --> 00:01:06,159
in rdi is df33 48 60.

30
00:01:04,159 --> 00:01:08,240
so if that memory address gets written

31
00:01:06,159 --> 00:01:10,320
to then it's going to break so i'm going

32
00:01:08,240 --> 00:01:12,640
to go ahead and run it again i'm

33
00:01:10,320 --> 00:01:15,439
actually going to set a

34
00:01:12,640 --> 00:01:17,119
set a bookmark so I can walk backwards

35
00:01:15,439 --> 00:01:19,920
after this occurs for a reason i'll show

36
00:01:17,119 --> 00:01:22,080
you in a second so I run it

37
00:01:19,920 --> 00:01:24,880
and it says breakpoint 2 fired because

38
00:01:22,080 --> 00:01:27,040
this memory address has been written to

39
00:01:24,880 --> 00:01:29,840
so let's go ahead and grab that address

40
00:01:27,040 --> 00:01:30,720
now it says jump here rather than you

41
00:01:29,840 --> 00:01:33,200
know

42
00:01:30,720 --> 00:01:35,119
saying this particular address this

43
00:01:33,200 --> 00:01:38,720
particular assembly instruction

44
00:01:35,119 --> 00:01:41,439
so as a reminder as we learned in

45
00:01:38,720 --> 00:01:43,119
architecture 2001

46
00:01:41,439 --> 00:01:45,520
hardware to bug break points which is

47
00:01:43,119 --> 00:01:47,840
what a break on right would be behave

48
00:01:45,520 --> 00:01:49,200
like a trap it's a trap

49
00:01:47,840 --> 00:01:52,240
so the

50
00:01:49,200 --> 00:01:55,040
faults so fault hitler's fault

51
00:01:52,240 --> 00:01:57,200
faults point at the rip of the assembly

52
00:01:55,040 --> 00:01:59,280
instruction that caused it whereas traps

53
00:01:57,200 --> 00:02:01,360
point to the next assembly instruction

54
00:01:59,280 --> 00:02:03,520
hardware breakpoints are traps not

55
00:02:01,360 --> 00:02:05,200
faults and therefore it is pointing at

56
00:02:03,520 --> 00:02:07,280
the assembly instruction after the one

57
00:02:05,200 --> 00:02:08,959
that actually caused it

58
00:02:07,280 --> 00:02:10,720
and so just to prove that you know you

59
00:02:08,959 --> 00:02:13,840
could look at the addresses i'm going to

60
00:02:10,720 --> 00:02:16,239
go into the GUI disassembly view

61
00:02:13,840 --> 00:02:18,080
and look at that now I see this jump

62
00:02:16,239 --> 00:02:19,520
instruction and it says before the legal

63
00:02:18,080 --> 00:02:21,200
instruction because it can't really

64
00:02:19,520 --> 00:02:22,720
disassembly because it just went you

65
00:02:21,200 --> 00:02:24,080
know subtracted by one because it

66
00:02:22,720 --> 00:02:26,319
doesn't really know where it is but

67
00:02:24,080 --> 00:02:27,680
that's why I set the break so I set the

68
00:02:26,319 --> 00:02:30,879
bookmark

69
00:02:27,680 --> 00:02:32,959
to allow me to unstep and walk backwards

70
00:02:30,879 --> 00:02:35,200
right once I do that then it reverts the

71
00:02:32,959 --> 00:02:36,959
state and says oh yeah that was address

72
00:02:35,200 --> 00:02:39,120
9644c

73
00:02:36,959 --> 00:02:41,120
which actually or sorry this address

74
00:02:39,120 --> 00:02:43,760
right here that's the bookmark this is

75
00:02:41,120 --> 00:02:45,599
the instruction pointer so

76
00:02:43,760 --> 00:02:47,760
9644e

77
00:02:45,599 --> 00:02:49,519
is the place where the actual right to

78
00:02:47,760 --> 00:02:51,840
that address was there

79
00:02:49,519 --> 00:02:54,000
and you know you could also do this kind

80
00:02:51,840 --> 00:02:55,920
of thing by checking the

81
00:02:54,000 --> 00:02:57,360
memory address was actually written that

82
00:02:55,920 --> 00:02:59,200
kind of thing

83
00:02:57,360 --> 00:03:02,239
so the value before the assembly

84
00:02:59,200 --> 00:03:04,400
instruction ran is zero at this memory

85
00:03:02,239 --> 00:03:06,159
address and if I allow it to step back

86
00:03:04,400 --> 00:03:08,159
forward again so that it executed the

87
00:03:06,159 --> 00:03:10,000
assembly instruction then you can see

88
00:03:08,159 --> 00:03:11,519
that it's actually 3f has been written

89
00:03:10,000 --> 00:03:14,959
to this and that's what causes the

90
00:03:11,519 --> 00:03:14,959
breakpoint to fire

