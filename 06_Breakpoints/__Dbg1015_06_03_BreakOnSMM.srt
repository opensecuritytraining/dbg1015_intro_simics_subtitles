1
00:00:00,240 --> 00:00:04,560
all right now another type of special

2
00:00:02,480 --> 00:00:07,040
break point that we might want to set is

3
00:00:04,560 --> 00:00:08,800
a break on entry to system management

4
00:00:07,040 --> 00:00:10,880
mode which you'll learn about in

5
00:00:08,800 --> 00:00:14,719
architecture 4001.

6
00:00:10,880 --> 00:00:17,119
so if I set that and also set a bookmark

7
00:00:14,719 --> 00:00:19,439
so that I can walk backwards I can just

8
00:00:17,119 --> 00:00:22,960
go ahead and run the code and it'll

9
00:00:19,439 --> 00:00:25,359
break once it has entered into smm

10
00:00:22,960 --> 00:00:27,119
now to walk backwards and see what's

11
00:00:25,359 --> 00:00:29,840
actually causing that

12
00:00:27,119 --> 00:00:31,519
i can go ahead and go into the

13
00:00:29,840 --> 00:00:33,760
disassembly view

14
00:00:31,519 --> 00:00:36,160
and unstep

15
00:00:33,760 --> 00:00:38,559
and unstep a little bit more

16
00:00:36,160 --> 00:00:40,559
and I can basically see you know this is

17
00:00:38,559 --> 00:00:43,280
the assembly code which is actually

18
00:00:40,559 --> 00:00:44,719
causing the system to enter into smm

19
00:00:43,280 --> 00:00:46,559
mode

20
00:00:44,719 --> 00:00:48,559
and if I then allow the thing to just

21
00:00:46,559 --> 00:00:52,520
run again it would again break once it

22
00:00:48,559 --> 00:00:52,520
enters into smm

