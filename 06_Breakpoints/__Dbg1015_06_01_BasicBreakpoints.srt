1
00:00:00,160 --> 00:00:03,760
now to set breakpoints in Simics you're

2
00:00:02,080 --> 00:00:05,440
going to use the break command i'm

3
00:00:03,760 --> 00:00:08,080
actually going to grab one of the

4
00:00:05,440 --> 00:00:09,200
addresses from a previous

5
00:00:08,080 --> 00:00:12,160
breakpoint

6
00:00:09,200 --> 00:00:14,799
and i'm going to load the target

7
00:00:12,160 --> 00:00:17,119
i will execute break and then that

8
00:00:14,799 --> 00:00:19,039
previous address which I had

9
00:00:17,119 --> 00:00:21,279
broken at just randomly

10
00:00:19,039 --> 00:00:23,039
so this will set a breakpoint I can see

11
00:00:21,279 --> 00:00:25,199
which breakpoints there are by doing

12
00:00:23,039 --> 00:00:26,880
list breakpoints

13
00:00:25,199 --> 00:00:30,240
and so that shows I have a single one

14
00:00:26,880 --> 00:00:32,000
right now and I could go ahead and run

15
00:00:30,240 --> 00:00:33,920
and it should run and eventually hit

16
00:00:32,000 --> 00:00:36,160
that breakpoint at which point I could

17
00:00:33,920 --> 00:00:38,239
disassemble to see where I am so there

18
00:00:36,160 --> 00:00:40,640
you go move eax

19
00:00:38,239 --> 00:00:42,879
which should match what I saw up here

20
00:00:40,640 --> 00:00:45,600
move eax

21
00:00:42,879 --> 00:00:46,559
and you can also remove breakpoints with

22
00:00:45,600 --> 00:00:48,879
delete

23
00:00:46,559 --> 00:00:51,120
and then a particular number and id

24
00:00:48,879 --> 00:00:52,960
given by list breakpoints

25
00:00:51,120 --> 00:00:57,480
and so now if I do list breakpoints

26
00:00:52,960 --> 00:00:57,480
again I shouldn't see any breakpoints

