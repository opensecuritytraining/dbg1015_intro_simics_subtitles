1
00:00:00,080 --> 00:00:03,120
let's take a look at the basics of the

2
00:00:01,760 --> 00:00:05,520
Simics gui

3
00:00:03,120 --> 00:00:07,759
so you can load just Simics.bat without

4
00:00:05,520 --> 00:00:09,599
a target and you'll see that the Simics

5
00:00:07,759 --> 00:00:11,440
console comes up

6
00:00:09,599 --> 00:00:13,040
now the most interesting things to us

7
00:00:11,440 --> 00:00:15,759
are going to be under the debug menu so

8
00:00:13,040 --> 00:00:18,000
disassembly cpu registers etc but you

9
00:00:15,759 --> 00:00:20,160
can see it's not actually possible to

10
00:00:18,000 --> 00:00:21,279
open those yet until we run an actual

11
00:00:20,160 --> 00:00:24,160
command file

12
00:00:21,279 --> 00:00:28,000
so let's go ahead and run command file

13
00:00:24,160 --> 00:00:29,760
targets qsp-x86 first step simx

14
00:00:28,000 --> 00:00:32,079
when we do that a bunch of windows are

15
00:00:29,760 --> 00:00:34,480
going to pop up so let's look at those

16
00:00:32,079 --> 00:00:36,640
first one is the serial console that's

17
00:00:34,480 --> 00:00:38,559
going to be basically serial output that

18
00:00:36,640 --> 00:00:40,239
occurs as the system is booting whether

19
00:00:38,559 --> 00:00:41,440
it's from the firmware or the operating

20
00:00:40,239 --> 00:00:43,760
system

21
00:00:41,440 --> 00:00:45,680
there's a sort of windows window that

22
00:00:43,760 --> 00:00:48,160
shows you what available windows are up

23
00:00:45,680 --> 00:00:50,239
so there can be two serial consoles only

24
00:00:48,160 --> 00:00:52,320
one of which is up right now and then

25
00:00:50,239 --> 00:00:54,079
also there's this graphics console and

26
00:00:52,320 --> 00:00:56,160
this will actually show you you know

27
00:00:54,079 --> 00:00:58,719
what's actually booting as it's booting

28
00:00:56,160 --> 00:01:01,120
and the GUI once a full operating system

29
00:00:58,719 --> 00:01:01,120
comes up

30
00:01:01,920 --> 00:01:06,560
so if we go back into the Simics control

31
00:01:04,239 --> 00:01:08,799
window at this point now we can see that

32
00:01:06,560 --> 00:01:10,960
things like disassembly cpu registers

33
00:01:08,799 --> 00:01:13,600
memory contents are available so let's

34
00:01:10,960 --> 00:01:15,040
go ahead and select disassembly and what

35
00:01:13,600 --> 00:01:16,640
you're going to see because you haven't

36
00:01:15,040 --> 00:01:19,119
actually run anything yet you've just

37
00:01:16,640 --> 00:01:21,520
started it up but not started it running

38
00:01:19,119 --> 00:01:23,439
you'll see what is effectively the reset

39
00:01:21,520 --> 00:01:26,240
vector the starting point for all code

40
00:01:23,439 --> 00:01:28,479
on this simulated cpu and it's no up no

41
00:01:26,240 --> 00:01:31,280
up and then a backwards jump and we'll

42
00:01:28,479 --> 00:01:34,000
learn about that in architecture 4001

43
00:01:31,280 --> 00:01:36,159
we can also bring up the cpu registers

44
00:01:34,000 --> 00:01:38,000
window and this will show you your

45
00:01:36,159 --> 00:01:40,799
general purpose registers that you

46
00:01:38,000 --> 00:01:42,720
learned about in architecture 1001 as

47
00:01:40,799 --> 00:01:43,520
well as eflags register

48
00:01:42,720 --> 00:01:46,880
and

49
00:01:43,520 --> 00:01:48,880
it also will there's also the system

50
00:01:46,880 --> 00:01:50,240
registers view which shows you the type

51
00:01:48,880 --> 00:01:54,479
of registers that you learned about in

52
00:01:50,240 --> 00:01:56,640
architecture 2001 the idtr gdtr the

53
00:01:54,479 --> 00:02:00,000
segment selector registers and the

54
00:01:56,640 --> 00:02:02,399
control register 0 3 and 4.

55
00:02:00,000 --> 00:02:04,719
we can also bring up from the debug menu

56
00:02:02,399 --> 00:02:07,520
a memory contents window

57
00:02:04,719 --> 00:02:09,759
and here i'm going to put in an address

58
00:02:07,520 --> 00:02:12,560
one two three four

59
00:02:09,759 --> 00:02:13,560
one two three four

60
00:02:12,560 --> 00:02:15,120
so

61
00:02:13,560 --> 00:02:19,520
ffffff00

62
00:02:15,120 --> 00:02:22,480
and this right here this fff f0

63
00:02:19,520 --> 00:02:26,480
is 9090 e8

64
00:02:22,480 --> 00:02:28,560
eb c4 999 to 90 and that actually is

65
00:02:26,480 --> 00:02:30,080
this code right here so again that's the

66
00:02:28,560 --> 00:02:32,720
kind of thing you'll learn about in the

67
00:02:30,080 --> 00:02:34,800
architecture 4001 class but this is just

68
00:02:32,720 --> 00:02:37,040
to say that in general you know the

69
00:02:34,800 --> 00:02:39,599
basic GUI stuff that we'd be interested

70
00:02:37,040 --> 00:02:42,080
in is a disassembly view

71
00:02:39,599 --> 00:02:44,080
a capability to see registers whether

72
00:02:42,080 --> 00:02:45,599
their general purpose or system and a

73
00:02:44,080 --> 00:02:49,200
memory contents view

74
00:02:45,599 --> 00:02:50,560
now because the GUI is sort of a minimal

75
00:02:49,200 --> 00:02:52,640
afterthought

76
00:02:50,560 --> 00:02:54,000
there's no way to for instance change

77
00:02:52,640 --> 00:02:56,160
the grouping there's no way to change

78
00:02:54,000 --> 00:02:58,480
this to like four byte you know big

79
00:02:56,160 --> 00:03:00,000
endian view or anything like that so

80
00:02:58,480 --> 00:03:01,440
most of the things that are complicated

81
00:03:00,000 --> 00:03:04,000
you'll have to do from the command line

82
00:03:01,440 --> 00:03:06,319
view but still when it comes to a basic

83
00:03:04,000 --> 00:03:08,480
capability to step through the assembly

84
00:03:06,319 --> 00:03:10,319
instructions for instance and you know

85
00:03:08,480 --> 00:03:14,319
see what's going on with the registers

86
00:03:10,319 --> 00:03:14,319
this does give you that capability

