1
00:00:00,080 --> 00:00:04,720
now let's launch Simics again and see

2
00:00:02,399 --> 00:00:05,759
how to run the code both backwards and

3
00:00:04,720 --> 00:00:07,759
forwards

4
00:00:05,759 --> 00:00:10,160
so as it indicates here there's the run

5
00:00:07,759 --> 00:00:11,679
and stop commands and so if you just go

6
00:00:10,160 --> 00:00:14,000
ahead and hit run

7
00:00:11,679 --> 00:00:15,679
what you'll see is that the GUI window

8
00:00:14,000 --> 00:00:18,320
will show that it's doing some sort of

9
00:00:15,679 --> 00:00:21,039
uefi boot through the firmware

10
00:00:18,320 --> 00:00:22,160
and what it'll ultimately be doing is

11
00:00:21,039 --> 00:00:25,760
launching

12
00:00:22,160 --> 00:00:27,680
a intel clear linux distribution

13
00:00:25,760 --> 00:00:30,720
and in the GUI window it'll show a login

14
00:00:27,680 --> 00:00:33,440
prompt and in the serial output window

15
00:00:30,720 --> 00:00:36,480
it'll show that it will automatically

16
00:00:33,440 --> 00:00:36,480
log into the system

17
00:00:40,320 --> 00:00:44,239
so here you can see that it

18
00:00:42,160 --> 00:00:46,960
automatically logged into the system and

19
00:00:44,239 --> 00:00:48,480
if I type ls-la

20
00:00:46,960 --> 00:00:50,559
i will see that you know it's a typical

21
00:00:48,480 --> 00:00:53,440
linux system it doesn't automatically

22
00:00:50,559 --> 00:00:56,719
log into the GUI but you can do that by

23
00:00:53,440 --> 00:00:59,120
just typing Simics as a username

24
00:00:56,719 --> 00:01:01,440
so now let's go ahead and exit that and

25
00:00:59,120 --> 00:01:03,600
let's see reverse execution instead and

26
00:01:01,440 --> 00:01:05,119
let's also see stopping in the middle of

27
00:01:03,600 --> 00:01:08,159
execution

28
00:01:05,119 --> 00:01:10,880
so for instance I could run run and then

29
00:01:08,159 --> 00:01:12,880
give it a second and then stop and what

30
00:01:10,880 --> 00:01:14,400
i'm now going to see is that it's

31
00:01:12,880 --> 00:01:17,280
somewhere in the middle of code

32
00:01:14,400 --> 00:01:18,720
execution so you can see that the gui

33
00:01:17,280 --> 00:01:20,159
has started booting but now it's

34
00:01:18,720 --> 00:01:22,400
actually stopped

35
00:01:20,159 --> 00:01:24,080
and if I look at the disassembly window

36
00:01:22,400 --> 00:01:27,280
you can see that it's you know somewhere

37
00:01:24,080 --> 00:01:29,520
in somewhere else in the execution

38
00:01:27,280 --> 00:01:31,840
so what I want to show above and beyond

39
00:01:29,520 --> 00:01:33,520
that is the capability to reverse

40
00:01:31,840 --> 00:01:36,240
execution

41
00:01:33,520 --> 00:01:38,000
so if I launch it again

42
00:01:36,240 --> 00:01:39,840
if I set a bookmark at the beginning

43
00:01:38,000 --> 00:01:42,320
that basically tells Simics like

44
00:01:39,840 --> 00:01:44,479
starting at this location start tracking

45
00:01:42,320 --> 00:01:46,240
all of the state changes

46
00:01:44,479 --> 00:01:48,399
and then it uses that in order to

47
00:01:46,240 --> 00:01:49,920
reverse the state changes when you want

48
00:01:48,399 --> 00:01:52,560
to run in reverse

49
00:01:49,920 --> 00:01:56,640
so here if I run with a bookmark

50
00:01:52,560 --> 00:01:58,640
and then I stop and I can look at my

51
00:01:56,640 --> 00:02:00,320
you know particular state in the

52
00:01:58,640 --> 00:02:02,079
disassembly window

53
00:02:00,320 --> 00:02:04,240
so again you can see it's you know at

54
00:02:02,079 --> 00:02:07,040
some return assembly instruction this

55
00:02:04,240 --> 00:02:10,080
time there's an unstep option so I can

56
00:02:07,040 --> 00:02:11,920
step forward but I can also unstep which

57
00:02:10,080 --> 00:02:14,160
will step backwards

58
00:02:11,920 --> 00:02:17,360
and furthermore I can reverse the entire

59
00:02:14,160 --> 00:02:19,280
execution by typing reverse

60
00:02:17,360 --> 00:02:21,680
and now you saw that the you know

61
00:02:19,280 --> 00:02:24,319
disassembly was somewhere else in code

62
00:02:21,680 --> 00:02:26,080
execution if I reverse because I set to

63
00:02:24,319 --> 00:02:27,280
the initial bookmark at the very very

64
00:02:26,080 --> 00:02:29,840
beginning

65
00:02:27,280 --> 00:02:31,280
then this should walk myself all the way

66
00:02:29,840 --> 00:02:34,480
back to the very beginning of the

67
00:02:31,280 --> 00:02:34,480
assembly code

