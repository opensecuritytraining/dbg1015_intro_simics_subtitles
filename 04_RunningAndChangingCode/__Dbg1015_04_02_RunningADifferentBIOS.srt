1
00:00:00,320 --> 00:00:04,319
okay to see how to

2
00:00:02,159 --> 00:00:06,080
start a different BIOS image just go

3
00:00:04,319 --> 00:00:08,080
ahead and start cmx.pat without a

4
00:00:06,080 --> 00:00:11,280
command line argument then you're going

5
00:00:08,080 --> 00:00:12,960
to want to provide the run command file

6
00:00:11,280 --> 00:00:15,519
a particular target doesn't really

7
00:00:12,960 --> 00:00:18,400
matter which and then a BIOS underscore

8
00:00:15,519 --> 00:00:20,960
image equals in some particular BIOS

9
00:00:18,400 --> 00:00:24,160
file so here i've taken the last four

10
00:00:20,960 --> 00:00:25,439
megabytes of the dell optiplex 7010

11
00:00:24,160 --> 00:00:26,320
image

12
00:00:25,439 --> 00:00:28,160
so

13
00:00:26,320 --> 00:00:29,920
before I actually okay so i'm going to

14
00:00:28,160 --> 00:00:32,079
run this without that first just so that

15
00:00:29,920 --> 00:00:34,239
we can you know see the difference

16
00:00:32,079 --> 00:00:36,719
so if I run without that and then I go

17
00:00:34,239 --> 00:00:39,680
look at the disassembly view i'm going

18
00:00:36,719 --> 00:00:41,360
to see the typical knob knob jump

19
00:00:39,680 --> 00:00:42,879
backwards and a bunch of knobs that we

20
00:00:41,360 --> 00:00:45,440
expect

21
00:00:42,879 --> 00:00:48,559
then i'm going to go ahead and start it

22
00:00:45,440 --> 00:00:50,960
with that bio custom BIOS image

23
00:00:48,559 --> 00:00:54,239
and what I expect to see is some

24
00:00:50,960 --> 00:00:57,280
different assembly at the reset vector

25
00:00:54,239 --> 00:00:59,359
so go into the sims control go to

26
00:00:57,280 --> 00:01:01,280
disassembly and now I see something

27
00:00:59,359 --> 00:01:03,920
different knob knob jump but then

28
00:01:01,280 --> 00:01:04,879
there's all these add bite things off to

29
00:01:03,920 --> 00:01:08,000
the side

30
00:01:04,879 --> 00:01:10,240
so if I step through to this jump I also

31
00:01:08,000 --> 00:01:12,240
see significantly different assembly and

32
00:01:10,240 --> 00:01:14,320
so this proves to me that i'm actually

33
00:01:12,240 --> 00:01:17,200
in the different binary file that i

34
00:01:14,320 --> 00:01:20,560
passed in not in the default binary file

35
00:01:17,200 --> 00:01:20,560
that comes with Simics

