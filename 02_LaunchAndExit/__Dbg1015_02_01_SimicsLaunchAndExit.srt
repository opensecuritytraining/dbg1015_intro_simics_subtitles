1
00:00:00,240 --> 00:00:04,400
there are two ways to launch Simics

2
00:00:02,480 --> 00:00:07,200
the first is what we'll use through most

3
00:00:04,400 --> 00:00:09,519
of the class which is to launch cimx.bat

4
00:00:07,200 --> 00:00:12,280
and then a specific target

5
00:00:09,519 --> 00:00:14,719
so targets qsp-x86

6
00:00:12,280 --> 00:00:16,400
firststeps.cmix is the typical beginner

7
00:00:14,719 --> 00:00:18,160
one although we'll use more complicated

8
00:00:16,400 --> 00:00:20,080
ones later

9
00:00:18,160 --> 00:00:21,680
if you hit enter then it'll go ahead and

10
00:00:20,080 --> 00:00:23,760
launch the mix a bunch of windows will

11
00:00:21,680 --> 00:00:26,240
come up and the command line prompt will

12
00:00:23,760 --> 00:00:29,760
change to a Simics prompt from there you

13
00:00:26,240 --> 00:00:31,840
can exit cmx by typing exit

14
00:00:29,760 --> 00:00:35,120
now the other way that you can do it is

15
00:00:31,840 --> 00:00:37,120
to just launch cimx.bat by itself

16
00:00:35,120 --> 00:00:38,559
and then from within the Simics prompt

17
00:00:37,120 --> 00:00:40,719
you can run

18
00:00:38,559 --> 00:00:42,160
the command run

19
00:00:40,719 --> 00:00:43,680
command file

20
00:00:42,160 --> 00:00:46,399
and then the specific target that you

21
00:00:43,680 --> 00:00:47,920
want to use the main benefit to this is

22
00:00:46,399 --> 00:00:49,920
when you're getting started with Simics

23
00:00:47,920 --> 00:00:53,280
you can use tab completion in order to

24
00:00:49,920 --> 00:00:55,360
see what other options are available

25
00:00:53,280 --> 00:00:58,079
so this shows for instance there's a

26
00:00:55,360 --> 00:01:00,800
BIOS image option which would override

27
00:00:58,079 --> 00:01:03,359
what's actually used for the firmware

28
00:01:00,800 --> 00:01:06,479
things like the disk size

29
00:01:03,359 --> 00:01:09,520
machine name ip address frequency of the

30
00:01:06,479 --> 00:01:10,799
cpu number of cpu cores and so forth so

31
00:01:09,520 --> 00:01:13,439
there's a bunch of different things that

32
00:01:10,799 --> 00:01:14,880
can be used to customize the launch of

33
00:01:13,439 --> 00:01:17,040
the simulation

34
00:01:14,880 --> 00:01:19,600
but in the same way if you just hit

35
00:01:17,040 --> 00:01:22,159
enter then it will go ahead and start up

36
00:01:19,600 --> 00:01:24,000
Simics and be ready to run

37
00:01:22,159 --> 00:01:26,479
but we just want to exit for now and

38
00:01:24,000 --> 00:01:29,960
we'll come back to understanding the gui

39
00:01:26,479 --> 00:01:29,960
in just a second

