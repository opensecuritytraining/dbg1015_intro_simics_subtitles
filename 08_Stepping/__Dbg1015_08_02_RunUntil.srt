1
00:00:00,480 --> 00:00:04,400
all right so this time i'm going to

2
00:00:02,080 --> 00:00:06,000
actually follow what was listed on the

3
00:00:04,400 --> 00:00:07,600
page and i'm going to break on the

4
00:00:06,000 --> 00:00:09,840
fields

5
00:00:07,600 --> 00:00:12,080
so let's go ahead and break there run to

6
00:00:09,840 --> 00:00:14,719
that disassemble three assembly

7
00:00:12,080 --> 00:00:16,480
instructions at rip

8
00:00:14,719 --> 00:00:18,080
and we see you know we've broke there

9
00:00:16,480 --> 00:00:20,000
now we want to set a one-time break

10
00:00:18,080 --> 00:00:22,720
point on this address

11
00:00:20,000 --> 00:00:26,080
so let's go ahead and do

12
00:00:22,720 --> 00:00:26,080
break location

13
00:00:26,960 --> 00:00:31,560
and then

14
00:00:28,240 --> 00:00:31,560
this address

15
00:00:31,760 --> 00:00:36,079
and then dash x to say that it's an

16
00:00:34,079 --> 00:00:38,079
execute breakpoint break on execute and

17
00:00:36,079 --> 00:00:40,079
then dash once in order to say it's a

18
00:00:38,079 --> 00:00:42,239
one-time breakpoint

19
00:00:40,079 --> 00:00:44,640
so we do that and it says it planted it

20
00:00:42,239 --> 00:00:46,239
successfully you'll see that if we list

21
00:00:44,640 --> 00:00:47,360
our breakpoints we're not actually going

22
00:00:46,239 --> 00:00:49,760
to see it in the normal set of

23
00:00:47,360 --> 00:00:50,800
breakpoints only that first one but if

24
00:00:49,760 --> 00:00:53,440
we run

25
00:00:50,800 --> 00:00:55,199
it does break there and we can prove

26
00:00:53,440 --> 00:00:56,640
that it broke there by just

27
00:00:55,199 --> 00:00:58,480
disassembling

28
00:00:56,640 --> 00:01:00,320
a single assembly instruction and seeing

29
00:00:58,480 --> 00:01:03,320
that we are currently pointing at that

30
00:01:00,320 --> 00:01:03,320
location

