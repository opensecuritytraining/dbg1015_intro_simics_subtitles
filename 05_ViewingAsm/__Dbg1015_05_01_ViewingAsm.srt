1
00:00:00,399 --> 00:00:04,480
all right to see disassembly you can

2
00:00:02,639 --> 00:00:06,960
launch Simics and run a particular

3
00:00:04,480 --> 00:00:09,120
target so it'll be sitting there waiting

4
00:00:06,960 --> 00:00:11,519
and if you run the disassemble command

5
00:00:09,120 --> 00:00:13,280
or d a it will just disassemble

6
00:00:11,519 --> 00:00:15,679
automatically at the current instruction

7
00:00:13,280 --> 00:00:17,440
pointer if you hit it a couple of times

8
00:00:15,679 --> 00:00:20,160
you'll see that it just keeps stepping

9
00:00:17,440 --> 00:00:21,920
one instruction point or step forward to

10
00:00:20,160 --> 00:00:23,439
keep redisplaying you know different

11
00:00:21,920 --> 00:00:26,400
assembly

12
00:00:23,439 --> 00:00:28,640
additionally you can use the display

13
00:00:26,400 --> 00:00:31,920
command

14
00:00:28,640 --> 00:00:34,160
along with a d a command in order to

15
00:00:31,920 --> 00:00:37,120
display assembly at some particular

16
00:00:34,160 --> 00:00:40,000
address in this case the address is rip

17
00:00:37,120 --> 00:00:43,040
and specify that you want 10 count of

18
00:00:40,000 --> 00:00:44,960
disks of instructions disassembled

19
00:00:43,040 --> 00:00:47,440
and so if you set that display now every

20
00:00:44,960 --> 00:00:49,680
time the code stops it'll actually

21
00:00:47,440 --> 00:00:51,920
display 10 assembly instructions so we

22
00:00:49,680 --> 00:00:54,480
can confirm that by running and then

23
00:00:51,920 --> 00:00:56,480
stopping and then just wherever randomly

24
00:00:54,480 --> 00:00:59,840
we stopped in the code execution it will

25
00:00:56,480 --> 00:01:02,640
display those instructions so run stop

26
00:00:59,840 --> 00:01:05,280
same thing and of course as shown before

27
00:01:02,640 --> 00:01:07,920
from the GUI you can go to the debug

28
00:01:05,280 --> 00:01:09,439
menu and disassembly and from here you

29
00:01:07,920 --> 00:01:11,760
could you know step

30
00:01:09,439 --> 00:01:15,439
and see the assembly instructions you

31
00:01:11,760 --> 00:01:19,040
can also run and stop and then you'll be

32
00:01:15,439 --> 00:01:21,759
at whatever new assembly you're at

33
00:01:19,040 --> 00:01:23,360
and also if you'd like to see the actual

34
00:01:21,759 --> 00:01:25,439
bytes that are associated with that

35
00:01:23,360 --> 00:01:28,560
disassembly there's no way to actually

36
00:01:25,439 --> 00:01:30,479
do that from within this GUI disassembly

37
00:01:28,560 --> 00:01:32,159
view and so consequently you have to do

38
00:01:30,479 --> 00:01:33,280
that from the command line so that can

39
00:01:32,159 --> 00:01:35,280
be done with

40
00:01:33,280 --> 00:01:36,720
disassemble

41
00:01:35,280 --> 00:01:37,680
settings

42
00:01:36,720 --> 00:01:39,280
on

43
00:01:37,680 --> 00:01:42,000
and so from now on when it shows

44
00:01:39,280 --> 00:01:43,920
disassembly it will show you the actual

45
00:01:42,000 --> 00:01:45,360
assembly bytes that

46
00:01:43,920 --> 00:01:46,399
actually encode that assembly

47
00:01:45,360 --> 00:01:48,960
instruction

48
00:01:46,399 --> 00:01:50,399
so again I could run and I could stop

49
00:01:48,960 --> 00:01:52,560
and you know it's still sitting here at

50
00:01:50,399 --> 00:01:55,200
the halted state so i'm going to restart

51
00:01:52,560 --> 00:01:57,520
that quick

52
00:01:55,200 --> 00:01:59,280
i'm going to do the

53
00:01:57,520 --> 00:02:01,840
disassemble

54
00:01:59,280 --> 00:02:01,840
settings

55
00:02:02,320 --> 00:02:04,640
on

56
00:02:04,719 --> 00:02:08,840
da

57
00:02:05,840 --> 00:02:08,840
display

58
00:02:10,399 --> 00:02:12,800
10

59
00:02:11,920 --> 00:02:13,840
run

60
00:02:12,800 --> 00:02:15,599
stop

61
00:02:13,840 --> 00:02:17,040
there you go there's the raw bytes that

62
00:02:15,599 --> 00:02:19,920
encode those particular assembly

63
00:02:17,040 --> 00:02:19,920
instructions

