1
00:00:00,24 --> 00:00:03,14
Alright well to display the registers as always,

2
00:00:03,14 --> 00:00:05,35
you could just look at them from the debug menu

3
00:00:05,35 --> 00:00:09,26
and the CPU registers for integers which is general purpose

4
00:00:09,26 --> 00:00:13,52
the system registers and additionally there's floating point and sse

5
00:00:13,52 --> 00:00:15,41
stuff that we haven't talked about in any of our

6
00:00:15,41 --> 00:00:16,05
classes

7
00:00:16,94 --> 00:00:19,82
So to see the same thing from the command line

8
00:00:20,02 --> 00:00:26,09
you can for instance use we'll start with reed Wreg

9
00:00:26,1 --> 00:00:28,01
and then for instance are A X

10
00:00:28,74 --> 00:00:29,26
Right?

11
00:00:29,26 --> 00:00:31,66
And let's try R D X

12
00:00:31,67 --> 00:00:34,38
So that gives us something that is in decimal format

13
00:00:34,39 --> 00:00:37,0
So that's why you might want to use the output

14
00:00:37,0 --> 00:00:39,94
rate IX and set it to 16 so that it

15
00:00:39,94 --> 00:00:42,26
defaults to six to hex

16
00:00:42,74 --> 00:00:44,75
And then if you do the same thing again you'll

17
00:00:44,75 --> 00:00:46,92
see the hex value personally

18
00:00:46,92 --> 00:00:51,36
I tend to use the print dash X with the

19
00:00:51,37 --> 00:00:51,9
percent

20
00:00:51,9 --> 00:00:56,23
And then the uh particular register that I'm interested in

21
00:00:56,24 --> 00:00:58,04
because that's a little bit more flexible

22
00:00:58,04 --> 00:01:01,11
You can quickly change between binary decimal,

23
00:01:01,12 --> 00:01:01,55
exa,

24
00:01:01,55 --> 00:01:03,05
decimal and everything else

25
00:01:03,64 --> 00:01:05,37
Now if you want to see a bunch of general

26
00:01:05,37 --> 00:01:06,9
purpose registers all at once,

27
00:01:06,91 --> 00:01:09,34
you can use p regs,

28
00:01:09,35 --> 00:01:12,96
pre eggs and that'll show you you know what execution

29
00:01:12,96 --> 00:01:15,66
mode you're in and then all the various register estate

30
00:01:16,14 --> 00:01:18,12
if you want to see a whole bunch of stuff

31
00:01:18,13 --> 00:01:21,29
Thank new P regs all and then it'll show you

32
00:01:21,3 --> 00:01:25,33
the uh it'll show you the general purpose registers like

33
00:01:25,34 --> 00:01:27,74
is shown on one view of the window and the

34
00:01:27,74 --> 00:01:31,31
gooey then it will transition into the system registers

35
00:01:31,31 --> 00:01:32,76
Like is shown in the other view,

36
00:01:32,94 --> 00:01:34,45
The segment registers,

37
00:01:34,45 --> 00:01:35,22
the I D t,

38
00:01:35,22 --> 00:01:35,71
r,

39
00:01:35,72 --> 00:01:38,66
g D t R and the control registers

40
00:01:38,67 --> 00:01:40,85
And then all those other registers that we said we

41
00:01:40,85 --> 00:01:41,66
haven't learned about

42
00:01:42,44 --> 00:01:45,47
If you want to actually set a register instead of

43
00:01:45,47 --> 00:01:46,72
reading a register there,

44
00:01:46,72 --> 00:01:49,22
you can use Right WREG and then,

45
00:01:49,22 --> 00:01:49,91
for instance,

46
00:01:49,92 --> 00:01:53,25
R E X and something like that

47
00:01:54,14 --> 00:01:59,4
And then if we do read rig R e X

48
00:01:59,41 --> 00:02:00,56
we'll get that back

