1
00:00:00,240 --> 00:00:05,279
okay so if we want to see a

2
00:00:02,560 --> 00:00:06,720
self-updating stack diagram we're not

3
00:00:05,279 --> 00:00:08,400
really going to be able to do that from

4
00:00:06,720 --> 00:00:11,280
within the GUI because again the

5
00:00:08,400 --> 00:00:13,120
limitations of this means that we can't

6
00:00:11,280 --> 00:00:15,120
like group different areas of memory in

7
00:00:13,120 --> 00:00:16,800
different granularities but also there

8
00:00:15,120 --> 00:00:20,160
doesn't seem to be support for putting

9
00:00:16,800 --> 00:00:22,640
in a symbolic name such as rsp so that

10
00:00:20,160 --> 00:00:24,080
the address just keeps following rsp as

11
00:00:22,640 --> 00:00:25,519
rsp changes

12
00:00:24,080 --> 00:00:28,400
so instead we're going to do that from

13
00:00:25,519 --> 00:00:30,880
the command line let's go ahead and find

14
00:00:28,400 --> 00:00:34,320
our random break address b as we've had

15
00:00:30,880 --> 00:00:36,800
before let's find our random or not our

16
00:00:34,320 --> 00:00:39,120
random but let's find our display of

17
00:00:36,800 --> 00:00:41,840
10 assembly instructions at our ip and

18
00:00:39,120 --> 00:00:46,480
then let's set up a new display command

19
00:00:41,840 --> 00:00:48,000
of examining memory at rsp minus

20
00:00:46,480 --> 00:00:50,480
x38

21
00:00:48,000 --> 00:00:52,960
and i'm going to display 40 because as

22
00:00:50,480 --> 00:00:55,360
it says on the web page you need to do

23
00:00:52,960 --> 00:00:57,039
rsp minus whatever you want minus 8

24
00:00:55,360 --> 00:01:00,079
because you want to include those last 8

25
00:00:57,039 --> 00:01:02,559
bytes that rsp points at

26
00:01:00,079 --> 00:01:04,320
we'll display that in a granularity of

27
00:01:02,559 --> 00:01:05,920
64 bits at a time

28
00:01:04,320 --> 00:01:07,920
and we're going to display it in little

29
00:01:05,920 --> 00:01:10,479
indian form

30
00:01:07,920 --> 00:01:12,000
oops and I forgot the quote

31
00:01:10,479 --> 00:01:12,880
all right so then we just go ahead and

32
00:01:12,000 --> 00:01:15,360
run

33
00:01:12,880 --> 00:01:16,880
we hit this familiar random location in

34
00:01:15,360 --> 00:01:20,240
memory and

35
00:01:16,880 --> 00:01:21,920
now you know let's go ahead and use the

36
00:01:20,240 --> 00:01:23,680
disable command which we haven't used

37
00:01:21,920 --> 00:01:25,520
before to disable that single break

38
00:01:23,680 --> 00:01:27,600
point that we have and so you can see

39
00:01:25,520 --> 00:01:29,759
that we have the assembly right here

40
00:01:27,600 --> 00:01:31,280
followed by the stack and what i'm going

41
00:01:29,759 --> 00:01:33,280
to claim is that every time you stop

42
00:01:31,280 --> 00:01:35,439
this will be updated we haven't learned

43
00:01:33,280 --> 00:01:37,520
how to step through the assembly yet so

44
00:01:35,439 --> 00:01:40,799
i'm just instead going to

45
00:01:37,520 --> 00:01:42,880
run and then stop all right get updated

46
00:01:40,799 --> 00:01:44,479
run and then stop

47
00:01:42,880 --> 00:01:46,720
okay well it didn't update again because

48
00:01:44,479 --> 00:01:49,520
we're in a halted state so let's let it

49
00:01:46,720 --> 00:01:51,600
run for a bit longer see the gui

50
00:01:49,520 --> 00:01:54,640
progress a little bit

51
00:01:51,600 --> 00:01:56,240
and then let's stop again okay now we're

52
00:01:54,640 --> 00:01:58,640
somewhere else and you know it has

53
00:01:56,240 --> 00:02:00,719
changed so the point is just like other

54
00:01:58,640 --> 00:02:02,880
displays every time you stop it's going

55
00:02:00,719 --> 00:02:04,320
to update it and of course this is most

56
00:02:02,880 --> 00:02:05,680
useful when you're single stepping

57
00:02:04,320 --> 00:02:08,799
through the code as we'll learn about in

58
00:02:05,680 --> 00:02:08,799
a little bit

