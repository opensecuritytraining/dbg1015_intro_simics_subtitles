1
00:00:00,320 --> 00:00:05,839
okay so now let's go examine some memory

2
00:00:02,960 --> 00:00:08,720
and we use the x command very similar to

3
00:00:05,839 --> 00:00:10,719
what is used in gdb except for the fact

4
00:00:08,720 --> 00:00:13,920
that it doesn't support the same

5
00:00:10,719 --> 00:00:15,360
formatting as gdb format strings

6
00:00:13,920 --> 00:00:17,520
now i'm going to set a breakpoint on

7
00:00:15,360 --> 00:00:20,560
that old break location the random

8
00:00:17,520 --> 00:00:23,760
location I picked before fyi simic

9
00:00:20,560 --> 00:00:26,800
supports unixisms like hitting control r

10
00:00:23,760 --> 00:00:28,160
for a reverse search back through your

11
00:00:26,800 --> 00:00:30,560
command history

12
00:00:28,160 --> 00:00:34,079
so I can set a breakpoint like that i

13
00:00:30,560 --> 00:00:35,680
can set a display like that and then i

14
00:00:34,079 --> 00:00:38,239
can just run

15
00:00:35,680 --> 00:00:41,040
okay so before we set a break on right

16
00:00:38,239 --> 00:00:43,120
to whatever address rdi happens to be

17
00:00:41,040 --> 00:00:45,360
now we know a little bit more so we can

18
00:00:43,120 --> 00:00:46,879
examine that memory specifically well

19
00:00:45,360 --> 00:00:47,920
first let's go ahead and set that break

20
00:00:46,879 --> 00:00:50,320
on right

21
00:00:47,920 --> 00:00:51,680
break at rdi

22
00:00:50,320 --> 00:00:53,840
on right

23
00:00:51,680 --> 00:00:55,360
and then let's use the examine command

24
00:00:53,840 --> 00:00:57,039
and again let's you know we don't you

25
00:00:55,360 --> 00:00:59,520
know we could look and see you know

26
00:00:57,039 --> 00:01:00,640
we've learned now how to display rdi so

27
00:00:59,520 --> 00:01:02,559
we could do

28
00:01:00,640 --> 00:01:04,799
print dash x

29
00:01:02,559 --> 00:01:07,360
rdi and get the address

30
00:01:04,799 --> 00:01:11,280
but it's easier to just use the address

31
00:01:07,360 --> 00:01:13,760
as is so let's do examine memory at rdi

32
00:01:11,280 --> 00:01:14,880
and then we want to say let's say 16

33
00:01:13,760 --> 00:01:17,600
bytes

34
00:01:14,880 --> 00:01:20,159
displayed eight bits at a time

35
00:01:17,600 --> 00:01:24,400
okay so this is the what everything

36
00:01:20,159 --> 00:01:27,360
looks like before this move eax ii rdi

37
00:01:24,400 --> 00:01:31,840
happens we can check now the

38
00:01:27,360 --> 00:01:31,840
eax to see what that is

39
00:01:32,000 --> 00:01:35,520
well it's 2 right now but in reality

40
00:01:34,079 --> 00:01:37,520
it's going to get whatever this gets

41
00:01:35,520 --> 00:01:39,280
from rbx so something's going to be

42
00:01:37,520 --> 00:01:41,280
loaded into eax and then it's going to

43
00:01:39,280 --> 00:01:43,280
be written there so let's find out what

44
00:01:41,280 --> 00:01:44,960
that something is by just going ahead

45
00:01:43,280 --> 00:01:46,079
and running and letting the break point

46
00:01:44,960 --> 00:01:48,479
fire

47
00:01:46,079 --> 00:01:51,840
all right so the breakpoint fired now

48
00:01:48,479 --> 00:01:52,880
what's eax it's actually 3f

49
00:01:51,840 --> 00:01:55,520
and

50
00:01:52,880 --> 00:01:57,360
that 3f was moved to memory so let's go

51
00:01:55,520 --> 00:02:00,719
ahead and examine memory

52
00:01:57,360 --> 00:02:03,040
rdi 16 bytes 8 bits at a time

53
00:02:00,719 --> 00:02:04,719
and indeed we can see that 3f was

54
00:02:03,040 --> 00:02:06,399
written to memory

55
00:02:04,719 --> 00:02:09,440
so now if we want to change the memory

56
00:02:06,399 --> 00:02:11,360
ourselves we could use set and then a

57
00:02:09,440 --> 00:02:12,239
particular address this time let's use

58
00:02:11,360 --> 00:02:14,720
the

59
00:02:12,239 --> 00:02:17,360
absolute address rather than the rdi so

60
00:02:14,720 --> 00:02:21,120
let's set at that particular address

61
00:02:17,360 --> 00:02:22,800
let's say hex ffff and then let's just

62
00:02:21,120 --> 00:02:25,599
do one byte and this will show you that

63
00:02:22,800 --> 00:02:27,840
it sort of truncates it down to one byte

64
00:02:25,599 --> 00:02:30,560
and if we do that and examine it again

65
00:02:27,840 --> 00:02:33,280
we can see that ff was written there

66
00:02:30,560 --> 00:02:35,920
if we set it to write two bytes

67
00:02:33,280 --> 00:02:37,840
then when we examine it we can see that

68
00:02:35,920 --> 00:02:41,680
was written there

69
00:02:37,840 --> 00:02:42,800
now let's set it to write four bytes

70
00:02:41,680 --> 00:02:43,920
make sure I have the right number of

71
00:02:42,800 --> 00:02:45,680
bytes

72
00:02:43,920 --> 00:02:48,640
because I want to show you what happens

73
00:02:45,680 --> 00:02:49,599
when you under specify a size

74
00:02:48,640 --> 00:02:51,040
so

75
00:02:49,599 --> 00:02:53,360
all right four bytes was written there

76
00:02:51,040 --> 00:02:55,599
but now let's go back to

77
00:02:53,360 --> 00:02:58,959
this and let's change it to four bytes

78
00:02:55,599 --> 00:03:01,360
so we've got a two byte value fff and

79
00:02:58,959 --> 00:03:03,599
we're saying write that as four bytes

80
00:03:01,360 --> 00:03:07,920
so what Simics is going to do is say well

81
00:03:03,599 --> 00:03:09,760
the 4x value of that is 0 0 0 0 fff and

82
00:03:07,920 --> 00:03:11,599
then it's going to write that 4 bytes to

83
00:03:09,760 --> 00:03:13,840
memory and so we expect these to turn

84
00:03:11,599 --> 00:03:16,720
into zeros

85
00:03:13,840 --> 00:03:19,200
and indeed those two bytes got set to 0

86
00:03:16,720 --> 00:03:22,640
because the 4 byte value of this is 0 0

87
00:03:19,200 --> 00:03:25,120
0 0 fff

88
00:03:22,640 --> 00:03:28,239
so that shows us how we can examine

89
00:03:25,120 --> 00:03:30,319
memory and set memory and as a reminder

90
00:03:28,239 --> 00:03:32,480
you can only do this sort of alternate

91
00:03:30,319 --> 00:03:35,040
grouping of memory

92
00:03:32,480 --> 00:03:36,560
with the command line not in the GUI so

93
00:03:35,040 --> 00:03:37,840
i was just playing it eight bytes at a

94
00:03:36,560 --> 00:03:39,680
time but let's say

95
00:03:37,840 --> 00:03:42,159
eight bits at a time let's say I change

96
00:03:39,680 --> 00:03:44,000
to 32 bits at a time now it's going to

97
00:03:42,159 --> 00:03:46,000
display 32 bits at a time which is not

98
00:03:44,000 --> 00:03:47,760
possible on the GUI right now but the

99
00:03:46,000 --> 00:03:49,440
problem with that is that it's not

100
00:03:47,760 --> 00:03:51,680
actually displaying it in big indian

101
00:03:49,440 --> 00:03:54,239
order like we would expect so you have

102
00:03:51,680 --> 00:03:56,480
to pass the dash l flag to say interpret

103
00:03:54,239 --> 00:03:58,239
memory as little little indian memory

104
00:03:56,480 --> 00:03:59,920
which is what intel is

105
00:03:58,239 --> 00:04:01,680
and then it'll go ahead and flip it

106
00:03:59,920 --> 00:04:03,519
around assume it's little endian and

107
00:04:01,680 --> 00:04:04,959
show the most significant bytes over

108
00:04:03,519 --> 00:04:08,159
here and the least significant bytes

109
00:04:04,959 --> 00:04:10,239
over here as is the layout of memory

110
00:04:08,159 --> 00:04:11,920
can't do that from the gui

111
00:04:10,239 --> 00:04:14,080
at least not right now

112
00:04:11,920 --> 00:04:16,479
you can always submit a feature request

113
00:04:14,080 --> 00:04:18,320
to the Simics folks but like I said the

114
00:04:16,479 --> 00:04:20,000
the GUI seems to be mostly an

115
00:04:18,320 --> 00:04:22,160
afterthought because I think most of

116
00:04:20,000 --> 00:04:24,080
them use it in the command line so

117
00:04:22,160 --> 00:04:27,479
there's no apparent way to change this

118
00:04:24,080 --> 00:04:27,479
grouping here

