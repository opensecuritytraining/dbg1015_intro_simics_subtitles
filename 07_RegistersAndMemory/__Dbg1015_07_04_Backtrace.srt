1
00:00:00,14 --> 00:00:00,63
Okay

2
00:00:00,63 --> 00:00:02,88
So to see a stack back trace,

3
00:00:02,88 --> 00:00:04,98
we're going to have to add an extra command that

4
00:00:04,98 --> 00:00:07,55
we haven't required thus far and that is

5
00:00:07,55 --> 00:00:07,96
enable-debugger

6
00:00:08,74 --> 00:00:11,87
So enable the debugger and then we will now have

7
00:00:11,87 --> 00:00:13,89
the option to use the back trace command

8
00:00:14,34 --> 00:00:16,52
So we can actually just run the code

9
00:00:16,52 --> 00:00:19,03
And because the run command is non blocking it'll just

10
00:00:19,03 --> 00:00:19,45
run

11
00:00:19,46 --> 00:00:22,5
And then we can actually just periodically hit

12
00:00:22,5 --> 00:00:23,13
bt

13
00:00:23,14 --> 00:00:27,47
And we will see where the stack back trace says

14
00:00:27,61 --> 00:00:30,86
the stack pointers pointing at any given point in time

15
00:00:30,87 --> 00:00:32,06
as the code is running

16
00:00:32,34 --> 00:00:34,37
And of course we could stop too and then we

17
00:00:34,37 --> 00:00:35,7
could do back trace and we could,

18
00:00:35,7 --> 00:00:35,94
you know,

19
00:00:35,94 --> 00:00:39,36
disassemble rip 10 so we could look around

20
00:00:39,74 --> 00:00:42,41
But the key point is that back trace could be

21
00:00:42,41 --> 00:00:44,86
run while the system is running or while it stopped

22
00:00:44,87 --> 00:00:45,82
It doesn't really matter

23
00:00:45,83 --> 00:00:49,29
But you always need to enable the debugger before you

24
00:00:49,29 --> 00:00:50,86
can use the back trace command

