1
00:00:00,640 --> 00:00:04,400
okay here's a basic view of how to

2
00:00:02,560 --> 00:00:06,799
install Simics at the time that this

3
00:00:04,400 --> 00:00:09,280
material is made you should always use

4
00:00:06,799 --> 00:00:11,599
the instructions on the page itself

5
00:00:09,280 --> 00:00:14,400
because this video could get out of date

6
00:00:11,599 --> 00:00:17,600
so you go to the Simics download page

7
00:00:14,400 --> 00:00:18,720
scroll down find the download button

8
00:00:17,600 --> 00:00:21,039
click that it's going to bring you

9
00:00:18,720 --> 00:00:22,880
through to a eula

10
00:00:21,039 --> 00:00:25,359
you don't need to register or anything

11
00:00:22,880 --> 00:00:25,359
like that

12
00:00:26,240 --> 00:00:30,720
once the eula pops up go ahead and

13
00:00:28,640 --> 00:00:32,320
scroll all the way down because you know

14
00:00:30,720 --> 00:00:34,480
you're not going to read it

15
00:00:32,320 --> 00:00:36,079
and continue to download

16
00:00:34,480 --> 00:00:38,000
then you should download whichever

17
00:00:36,079 --> 00:00:40,000
package is appropriate either linux or

18
00:00:38,000 --> 00:00:42,000
windows i'm obviously on a windows

19
00:00:40,000 --> 00:00:43,200
system so i'm going to use that also you

20
00:00:42,000 --> 00:00:46,800
should use

21
00:00:43,200 --> 00:00:48,480
21 24 unless the web page instructions

22
00:00:46,800 --> 00:00:50,559
say otherwise so there might be a newer

23
00:00:48,480 --> 00:00:52,000
version that fixes some bugs later but

24
00:00:50,559 --> 00:00:53,360
for now this is the version that this

25
00:00:52,000 --> 00:00:55,440
was tested on

26
00:00:53,360 --> 00:01:00,320
so you're going to download both of

27
00:00:55,440 --> 00:01:00,320
these elements for the windows version

28
00:01:03,680 --> 00:01:08,080
as that's downloading you can go ahead

29
00:01:05,519 --> 00:01:09,760
and open up the installation guide so

30
00:01:08,080 --> 00:01:12,320
that you can see what you'll be doing

31
00:01:09,760 --> 00:01:13,920
roughly

32
00:01:12,320 --> 00:01:15,840
once the download is done follow the

33
00:01:13,920 --> 00:01:17,600
steps for your particular platform so

34
00:01:15,840 --> 00:01:20,759
i'm on windows so i'm going to launch

35
00:01:17,600 --> 00:01:20,759
the exe

36
00:01:26,080 --> 00:01:30,479
on the more info and select run anyway

37
00:01:32,720 --> 00:01:36,960
i'm going to install this just for me

38
00:01:35,520 --> 00:01:41,320
i don't have any other users on the

39
00:01:36,960 --> 00:01:41,320
system anyways then click install

40
00:01:41,840 --> 00:01:46,000
and after it's done

41
00:01:43,439 --> 00:01:48,479
allow it to run the intel cimex package

42
00:01:46,000 --> 00:01:48,479
manager

43
00:01:49,920 --> 00:01:55,840
allow this to use the default location

44
00:01:52,159 --> 00:01:55,840
by clicking save there

45
00:01:56,159 --> 00:01:59,840
and then here you're going to want to

46
00:01:57,600 --> 00:02:02,240
click browse for bundle and this is the

47
00:01:59,840 --> 00:02:04,960
other component that you downloaded so

48
00:02:02,240 --> 00:02:09,840
go to your downloads folder

49
00:02:04,960 --> 00:02:09,840
and point it at this simx package bundle

50
00:02:10,959 --> 00:02:13,760
then hit finish

51
00:02:18,560 --> 00:02:21,879
hit yes

52
00:02:25,520 --> 00:02:32,560
all right so once that's finished then

53
00:02:28,000 --> 00:02:32,560
you can go ahead and hit launch demo

54
00:02:33,760 --> 00:02:37,920
this is going to invoke Simics and

55
00:02:36,160 --> 00:02:40,800
you'll get like a command line prompt

56
00:02:37,920 --> 00:02:43,920
and it'll pick some particular

57
00:02:40,800 --> 00:02:45,920
default value of the

58
00:02:43,920 --> 00:02:47,280
code that gets loaded

59
00:02:45,920 --> 00:02:48,640
the key thing that we want to see in

60
00:02:47,280 --> 00:02:51,280
order to prove that it's actually

61
00:02:48,640 --> 00:02:53,599
working is you're going to select a bug

62
00:02:51,280 --> 00:02:56,319
and then disassembly and if you see

63
00:02:53,599 --> 00:02:58,720
valid disassembly then everything is

64
00:02:56,319 --> 00:03:00,400
good and your system is set up correctly

65
00:02:58,720 --> 00:03:02,840
you can go ahead and close it by going

66
00:03:00,400 --> 00:03:04,800
into the command line window and hitting

67
00:03:02,840 --> 00:03:06,800
exit

68
00:03:04,800 --> 00:03:08,319
you can also now go ahead and close the

69
00:03:06,800 --> 00:03:09,760
package manager because we're going to

70
00:03:08,319 --> 00:03:12,319
be launching this from the command line

71
00:03:09,760 --> 00:03:12,319
from now on

72
00:03:12,800 --> 00:03:16,480
in order to launch it from the command

73
00:03:14,319 --> 00:03:18,560
line you can just run a normal command

74
00:03:16,480 --> 00:03:21,200
prompt

75
00:03:18,560 --> 00:03:22,879
and you're going to change directory to

76
00:03:21,200 --> 00:03:25,440
in your home folder there will be a

77
00:03:22,879 --> 00:03:28,000
Simics projects and then the default will

78
00:03:25,440 --> 00:03:30,080
be my Simics project one

79
00:03:28,000 --> 00:03:33,840
and so from this directory if you run

80
00:03:30,080 --> 00:03:35,599
Simics.bat that will relaunch everything

81
00:03:33,840 --> 00:03:39,200
so that's how we're going to be doing it

82
00:03:35,599 --> 00:03:39,200
the rest of this class

